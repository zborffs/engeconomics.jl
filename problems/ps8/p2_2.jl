using EngEconomics, Roots, Plots, Colors, ColorSchemes
plotly()

# Given
stra = "Model T"
strb = "Model A"
Pa = 100 # First Cost of A in ($1000)
Pb = 150 # First Cost of B in ($1000)
na = 5 # Lifetime of A in (years)
nb = 5 # Lifetime of B in (years)
Aa = 50 # Annual Savings of A in ($1000/yr)
Ab = 62 # Annual Savings of B in ($1000/yr)
Sa = 20 # Salvage of A in ($1000)
Sb = 30 # Salvage of B in ($1000)

# Construct a break-even graph showin the PW of each alternative as a
# function of interest rates between 6-20%
i = collect(0.06:0.01:0.20)

PWa = -Pa .+ Aa * seriesPresentAmountFactor.(i, na) .+ Sa * presentWorthFactor.(i, na)
PWb = -Pb .+ Ab * seriesPresentAmountFactor.(i, nb) .+ Sb * presentWorthFactor.(i, nb)

plot(i, PWa, label=stra, xlabel="Interest Rate (6%-20%)",
	ylabel="Net Present Worth (\$1000)",
	title="Comparison of NPWs of Two Alternatives<br> from Different Interest Rates",
	legend=:outerbottomright,
	palette=cgrad(ColorSchemes.berlin.colors))
plot!(i, PWb, label=strb)

# Which is preferred at 8 percent
i8 = findall(x->x==0.08, i)[1]
if PWa[i8] > PWb[i8]
	println("$(stra) is preferred because its NPW is greater than that of $(strb)")
elseif PWa[i8] < PWb[i8]
	println("$(strb) is preferred because its NPW is greater than that of $(stra)")
else # must be equal
	println("Doesn't matter, the NPWs are equal here")
end

# Which is preferred at 16 percent
i16 = findall(x->x==0.16, i)[1]
if PWa[i16] > PWb[i16]
	println("$(stra) is preferred because its NPW is greater than that of $(strb)")
elseif PWa[i16] < PWb[i16]
	println("$(strb) is preferred because its NPW is greater than that of $(stra)")
else # must be equal
	println("Doesn't matter, the NPWs are equal here")
end

# Determine the breakeven interest rate
# - From the graph it's clearly between 0.10 and 0.125, so guess 0.11 initially
PWa_func(x) = -Pa .+ Aa * seriesPresentAmountFactor.(x, na) .+ Sa * presentWorthFactor.(x, na)
PWb_func(x) = -Pb .+ Ab * seriesPresentAmountFactor.(x, nb) .+ Sb * presentWorthFactor.(x, nb)
f(x) = PWa_func(x) - PWb_func(x)
iBreakEven = find_zero(f, 0.11)
println("The breakeven interest rate is $(iBreakEven)")
