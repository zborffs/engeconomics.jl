using Roots, EngEconomics

# Rearrange for A in terms of i, and substitute in for A in other equation to get i alone and solve for i
f(i) = (-3 * capitalRecoveryFactor(i, 10) + 1) * ((20 - presentWorthFactor(i, 10)) / (-3 + seriesPresentAmountFactor(i, 10))) + sinkingFundFactor(i, 10) - 4
i_star = find_zero(f, 0.10)

# Now determine the values for A and P
A = ((20 - presentWorthFactor(i_star, 10)) / (-3 + seriesPresentAmountFactor(i_star, 10)))
P = 3 * A

# Now we determine the IRR by setting this equation up such that the interest rate returned is the break even interest, i.e. there is no Positive PW
PW_func(i) = -P + A * seriesPresentAmountFactor(i, 10) + presentWorthFactor(i, 10)
irr = find_zero(PW_func, i)
