using EngEconomics, Roots, Plots, Colors, ColorSchemes
plotly()

# Given
F = 50
A = 7
i = 0.1

sensitivity = [0.9, 0.95, 1.0, 1.05, 1.10]

n_A = log.((F ./ (A * sensitivity) * i .+ 1) ) ./ log(1 + i)
n_i = log.((F / A * (i * sensitivity) .+ 1) ) ./ log.(1 .+ (i * sensitivity))

plot(sensitivity, n_A, label="Sensitivity of Annual",
	xlabel="Sensitivity (+/- 5%, 10%)", ylabel="Number of Years",
	title="Sensitivity of Various Parameters on Time to Save Up",
	palette=cgrad(ColorSchemes.tab10.colors),
	legend=:outerbottomright)
plot!(sensitivity, n_i, label="Sensitivity of Interest")
