using DataFrames, Gadfly, Dates

D1 = DataFrame(y = Int[1, 2, 3, 4],
    ylab=["Collect data", "Clean data", "Analyze data", "Write report"],
    x = Date.(["2017-04-01", "2018-04-01", "2018-06-01", "2019-04-01"]),
    xend = Date.(["2018-04-01", "2018-06-01", "2019-04-01", "2020-04-01"]),
    id = ["a","b","b","c"]
)
ylabdict = Dict(i=>D1[:ylab][i] for i in 1:4)

coord = Coord.cartesian(ymin=0.4, ymax=4.6)
p = plot(D1, coord,
    layer(x=:x, xend=:xend, y=:y, yend=:y, color=:id, Geom.segment, Theme(line_width=10mm)),
    Scale.y_continuous(labels=i->get(ylabdict,i,"")),
    Guide.xlabel("Time"), Guide.ylabel(""),
    Theme(key_position=:none)
)


D2 = DataFrame(y = Int[1, 2, 3, 4, 5, 6, 7],
	ylab = ["1", "2", "3", "4", "5", "6", "7"],
	xend = [])
