using EngEconomics, Plots, PrettyTables, Roots, LinearAlgebra

# Given
x1 = -(150 + 30 + 50) * 0.05 + 0.3 * -(35 + 30 + 50) + 0.65 * -(30 + 50)
x2 = -(150 + 5 + 50) * 0.15 + 0.45 * -(35 + 5 + 50) + 0.4 * -(5 + 50)

x3 = max(x1, x2)
x4 = 0.34 * -50 + 0.66 * x3
x5 = 0.55 * -150 + 0.35 * -35 + 0.1 * -0
