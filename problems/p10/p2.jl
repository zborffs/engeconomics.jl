using Plots, LinearAlgebra, EngEconomics, PrettyTables

# Given
p = [0.35, 0.55, 0.1] # high, medium, low
x = [500, 250, 150]
P = 550
n = 5
MARR = 0.12

NPW = -P .+ x * seriesPresentAmountFactor(MARR, n)
pNPW = p

ENPW = dot(NPW, pNPW)
