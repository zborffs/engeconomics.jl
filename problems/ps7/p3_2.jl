using EngEconomics, Roots, Plots

Pa = 15000 # Prinicpal Cost of A in ($1000)
Pb = 10000 # Principal Cost of B in ($1000)
OMa = 55 # Annual Operations and Maintenance Cost of A in ($1000)
OMb = 35 # Annual Operations and Maintenance Cost of B in ($1000)
Aa = 450 # Annual Benefits of A in ($1000)
Ab = 200 # Annual Benefits of B in ($1000)
i = 0.05
n = 30

# We don't take the "Do Nothing" Option here because it is not a "direct" project
# Order the projects from least initial cost to most initial cost
#   (1) B
#   (2) A
# Determine the EUAB and EUAC of both projects and use the modified B/C equation
EUABb = Ab - OMb
EUACb = Pb * capitalRecoveryFactor(i, n)
EUABa = Aa - OMa
EUACa = Pa * capitalRecoveryFactor(i, n)

ΔB = EUABb - EUABa
ΔC = EUACb - EUACa
ΔBC = ΔB / ΔC

if ΔBC < 1.0
	println("Because the Incremental Benefit Cost Analysis rendered a result < 1.0, we must choose the project with the smaller initial cost")
	println("- Project B Wins!")
else
	println("Because the Incremental Benefit Cost Analysis rendered a result >= 1.0, we must choose the project with the greater initial cost")
	println("- Project A Wins!")
end

# Updates to the disbenefits of project A and B
disAa = 400
disAb = 500

EUABb = Ab - OMb - disAb
EUACb = Pb * capitalRecoveryFactor(i, n)
EUABa = Aa - OMa - disAa
EUACa = Pa * capitalRecoveryFactor(i, n)

ΔB = EUABb - EUABa
ΔC = EUACb - EUACa
ΔBC = ΔB / ΔC

if ΔBC < 1.0
	println("Because the Incremental Benefit Cost Analysis rendered a result < 1.0, we must choose the project with the smaller initial cost")
	println("- Project B Wins!")
else
	println("Because the Incremental Benefit Cost Analysis rendered a result >= 1.0, we must choose the project with the greater initial cost")
	println("- Project A Wins!")
end
