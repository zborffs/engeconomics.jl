using Plots, LinearAlgebra, EngEconomics, PrettyTables

P = 300 #initial cost
A = 30 # annualy OM costs
S = 60 # salvage value
n = [5, 6, 7, 8, 9]
p = [0.25, 0.1, 0.15, 0.2, 0.3]
i = 0.1

# Find EUAC for the equipment
EUAC = -P * capitalRecoveryFactor.(i, n) .- A .+ S * sinkingFundFactor.(i, n)
E_EUAC = dot(p, EUAC)
