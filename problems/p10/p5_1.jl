using EngEconomics, Plots, PrettyTables, Roots, LinearAlgebra

# Given
p = [0.40, 0.35, 0.25]
@assert isapprox(sum(p), 1.0; atol=0.01)
aString = "Increase Production"
bstring = "Keep Production the Same"
a = [750, 350, 100]
b = [500, 400, 200]

aE = dot(p, a)
bE = dot(p, b)

choice = max(aE, bE)

if aE == choice
	println(aString)
else
	println(bString)
end
