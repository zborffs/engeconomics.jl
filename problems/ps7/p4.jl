using EngEconomics, Roots, Plots

# Use Declining Balance Method
P = 30
n = 5
k = 3
S = 5

d = 1 - (S / P)^(1 / n)
BV_k = P * (1 - d)^k
Δ = BV_k - S
