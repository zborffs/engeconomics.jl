using Plots, LinearAlgebra, EngEconomics, PrettyTables

# Given
x = [30, 40, 50, 60] # Calls/hour
p = [0.2, 0.4, 0.3, 0.1]
E = 50

dot(max.(E .- x, 0), p)
