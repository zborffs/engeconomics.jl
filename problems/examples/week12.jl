using Plots, LinearAlgebra, EngEconomics, PrettyTables

x = [8000, 5000, 10000]
p = [0.6, 0.3, 0.1]

MARR = 0.10
P = 25000
n = [6, 9]
pn = [2/3, 1/3]

# expectation
E = dot(x, p)
bar(x, p, barwidths=300, xlabel="Annual Benefits \$", ylabel="Probability")

NPW = -P .+ x * transpose(seriesPresentAmountFactor.(MARR, n))
NPWlist = reshape(NPW, :, 1)
ptot = p * transpose(pn)
ptotlist = reshape(ptot, :, 1)

Etot = dot(NPWlist, ptotlist)
bar(NPWlist, ptotlist, barwidths=500, xlabel="NPW \$", ylabel="Probability")
header = ["Joint Probability" "PW"]
data   = hcat(ptotlist, NPWlist)
pretty_table(data, header)

# Decision Tree
