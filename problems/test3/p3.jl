using EngEconomics, Roots, Plots, Colors, ColorSchemes
plotly()

# Given
stra = "Convenient Store"
strb = "Restaurant"
Pa = 2000 # Initial Cost of A in $1000
OMa = 600 # Annual Cost of A in $1000
Aa = 1000 # Annual Benefit of A in $1000
Pb = 3000 # Initial Cost of B in $1000
OMb = 800 # Annual cost of B in $1000
Ab = 1300 # Annual Benefit of B in $1000
n = 20
# S = BV_20
# Use declining Balance depreciation with a factor of d = 1/N

# a) Construct a break-even graph shoing the NPW of each option as a function of
#    MARR. Use MARR values of 5, 6, 7, 8, 9, 10
MARR = collect(0.05:0.01:0.10)
d = 1 / n
k = (1-d)^(n)
Sa = Pa * k
Sb = Pb * k

PWa = -Pa .+ (Aa - OMa) * seriesPresentAmountFactor.(MARR, n) .+ Sa * presentWorthFactor.(MARR, n)
PWb = -Pb .+ (Ab - OMb) * seriesPresentAmountFactor.(MARR, n) .+ Sb * presentWorthFactor.(MARR, n)

plot(MARR, PWa, title="Comparison of Sensitivities of Two Alts<br> NPWs to MARR",
	xlabel="MARR (5%-10%)", ylabel="NPW (\$1000)",
	label=stra, legend=:outerbottomright, marker=:circle)
plot!(MARR, PWb, label=strb, marker=:circle)

# b) From part (a), what is the break-even MARR
PWa_func(x) = -Pa + (Aa - OMa) * seriesPresentAmountFactor(x, n) + Sa * presentWorthFactor(x, n)
PWb_func(x) = -Pb + (Ab - OMb) * seriesPresentAmountFactor(x, n) + Sb * presentWorthFactor(x, n)
M(x) = PWa_func(x) - PWb_func(x)
MARRBreakEven = find_zero(M, 0.085)

# c) WHat is the preferred option at MARR of 7.5%
