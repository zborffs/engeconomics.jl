using EngEconomics, Roots, Plots
plotly();

P = 2000000 # Initial Cost
OM = 100000 # Annual Cost ($/year)
A = 820000 # Annual Benefits ($/year)
Adis = 400000 # Annual Disbenefits ($/year)
n = 20 # Lifetime
i = 0.08 # Interest Rate

# Determine the Benefit-Cost ratio
# 1.) Use Annual Worth Analysis and use the modified B/C equation
B = A - OM - Adis
C = P * capitalRecoveryFactor(i, n)
BCRatio = B / C

if BCRatio >= 1.0
	println("This is an economically favorable enterprise")
else
	println("This is NOT an economically favorable enterprise")
end
