using EngEconomics, Plots, PrettyTables, Roots, LinearAlgebra

parcelCap = 60000
A = 80000
n = 12
MARR = 0.12
i = MARR / n
x = [2.95, 3.25, 3.50, 4.0, 5.0]
p = [0.1, 0.35, 0.3, 0.15, 0.1]

NPW = seriesPresentAmountFactor(i, n) * (-A .+ parcelCap * x)
ENPW = dot(p, NPW)
