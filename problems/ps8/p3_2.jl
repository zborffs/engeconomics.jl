using EngEconomics, Roots, Plots, Colors, ColorSchemes
plotly()

# Given
# - savings only after 3rd year
A = 10 # Annual Savings after 3 years in $(1000/yr)
G = 1 # Gradient of Annual Savings after 3 years in $(1000/yr)
MARR = 0.15
sensitivity = [1-0.15, 1-0.075, 1.0, 1+0.075, 1+0.15]
offset = 3
n = 5

# Construct a sensitivity graph for increases and decreases in: A, G, & MARR
PW_A = (A * sensitivity .+ G * sinkingFundPaymentArithmetic(MARR, n + 1)) * uniformSeriesCompoundAmoundFactor(MARR, n + 1) * presentWorthFactor(MARR, n + offset)
PW_G = (A .+ (G * sensitivity) * sinkingFundPaymentArithmetic(MARR, n + 1)) * uniformSeriesCompoundAmoundFactor(MARR, n + 1) * presentWorthFactor(MARR, n + offset)
PW_MARR = (A .+ G * sinkingFundPaymentArithmetic.(sensitivity * MARR, n + 1)) .* uniformSeriesCompoundAmoundFactor.(sensitivity * MARR, n + 1) .* presentWorthFactor.(sensitivity * MARR, n + offset)
plot(sensitivity, PW_A, label="Annual Savings", marker=:circle,
	legend=:outerbottomright,
	xlabel="Sensitivity (+/- 7.5%, 15%)",
	ylabel="Present Worth (\$1000)",
	title="Sensitivity of PW to Various Parameter Changes")
plot!(sensitivity, PW_G, label="Gradient", marker=:circle)
plot!(sensitivity, PW_MARR, label="Interest Rate", marker=:circle)
