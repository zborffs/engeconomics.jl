using EngEconomics, Plots, PrettyTables, Roots, LinearAlgebra

p = [0.35, 0.55, 0.10] # Probabilities of Savings
x = [500., 250., 150.] # Annual Savings (1K$/yr)
P = 550 # initial Cost
n = 5
MARR = 0.12

# Find Expected Annual Worth
NPW = -P .+ x * seriesPresentAmountFactor(MARR, n)
ENPW = dot(NPW, p)
println("Expected Net Present Worth: \$$(round(ENPW * 1000; digits=2))")

# Construct a Decision Tree on the basis of the EV should BB approve the
# development of a new robot
# - yes
