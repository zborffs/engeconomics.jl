using EngEconomics, Roots, Plots, Colors, ColorSchemes
plotly()

# Given
i = 0.09
n = 10
Ap = 600
Pp = 2300
# Aq = ?
Pq = 4000

EUABp = Ap
EUACp = Pp * capitalRecoveryFactor(i, n)
EUACq = Pq * capitalRecoveryFactor(i, n)
BCp = EUABp / EUACp
EUABq = EUACq * BCp

BCq = EUABq / EUACq
