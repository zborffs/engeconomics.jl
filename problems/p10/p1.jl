using Plots, LinearAlgebra, EngEconomics, PrettyTables

# Given
x = [2.95, 3.25, 3.50, 4.0, 5.0]
p = [0.1, 0.35, 0.3, 0.15, 0.1]
MARR = 0.12
maxParcels = 60000
A = 80000
n = 12
i = MARR / n

NPW = (-A .+ x * maxParcels) * seriesPresentAmountFactor(i, n)
pNPW = p

E = dot(NPW, pNPW)
bar(NPW, pNPW, xlabel="NPW \$", ylabel="Probability")
