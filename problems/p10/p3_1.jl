using EngEconomics, Plots, PrettyTables, Roots, LinearAlgebra

# Given
p = [0.2, 0.4, 0.3, 0.1]
@assert isapprox(sum(p), 1.0; atol=1e-3)
x = [30, 40, 50, 60]
avgCalls = 50

y = max.(50 .- x, 0)
E = dot(y, p)
