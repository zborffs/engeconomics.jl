using EngEconomics, Roots, Plots, Colors, ColorSchemes
plotly()

# Given
Pa = 6800 # Initial Cost of A in $
Pb = 11200 # Initial Cost of B in $
Pc = 2400 # Initial Cost of C in $
Aa = 2800 - 1000 - 400# Annual Cost of A in $/yr
Ab = 4400 - 1400 - 600 # Annual Cost of B in $/yr
Ac = 800 - 400 - 200 # Annual Cost of C in $/yr
na = 10
nb = 5
nc = 15
MARR = 0.09

# Step 1.) Order from smallest to biggest initial price
EUABc = Ac
EUACc = Pc * capitalRecoveryFactor(MARR, nc)
ΔBC_cnothing = EUABc / EUACc

EUABa = Aa
EUACa = Pa * capitalRecoveryFactor(MARR, na)
ΔBC_anothing = EUABa / EUACc

EUABb = Ab
EUACb = Pb * capitalRecoveryFactor(MARR, nb)

ΔBab = EUABb - EUABa
ΔCab = EUACb - EUACa

ΔBC_ab = ΔBab / ΔCab
